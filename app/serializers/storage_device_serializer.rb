class StorageDeviceSerializer < ActiveModel::Serializer
  attributes :id, :name, :description, :size, :format
end
