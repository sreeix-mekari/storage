# frozen_string_literal: true

class StatusController < ApplicationController
  SUCCESS = 0
  ERROR = 1

  def ping
    render plain: 'pong'
  end

  def status
    db = check_db
    json = {
      db: db
    }
    render json: json, status: db[:status] == SUCCESS ? :ok : :service_unavailable
  end

  private

  def check_db
    start = Time.now
    status =  rand(10)== 5 ?  ERROR: SUCCESS
    { status: status, time: Time.now - start }
  end
end
