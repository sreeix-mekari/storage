class StorageDevicesController < ApplicationController
  before_action :set_storage_device, only: [:show, :update, :destroy]

  # GET /storage_devices
  def index
    @storage_devices = StorageDevice.all

    render json: @storage_devices
  end

  # GET /storage_devices/1
  def show
    render json: @storage_device
  end

  # POST /storage_devices
  def create
    @storage_device = StorageDevice.new(storage_device_params)

    if @storage_device.save
      render json: @storage_device, status: :created, location: @storage_device
    else
      render json: @storage_device.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /storage_devices/1
  def update
    if @storage_device.update(storage_device_params)
      render json: @storage_device
    else
      render json: @storage_device.errors, status: :unprocessable_entity
    end
  end

  # DELETE /storage_devices/1
  def destroy
    @storage_device.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_storage_device
      @storage_device = StorageDevice.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def storage_device_params
      params.require(:storage_device).permit(:name, :description, :size, :format)
    end
end
