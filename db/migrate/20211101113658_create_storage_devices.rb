class CreateStorageDevices < ActiveRecord::Migration[6.1]
  def change
    create_table :storage_devices do |t|
      t.string :name
      t.string :description
      t.string :size
      t.string :format

      t.timestamps
    end
  end
end
