FactoryBot.define do
  factory :storage_device do
    name { "MyString" }
    description { "MyString" }
    size { "MyString" }
    format { "MyString" }
  end
end
