require "test_helper"

class StorageDevicesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @storage_device = storage_devices(:one)
  end

  test "should get index" do
    get storage_devices_url, as: :json
    assert_response :success
  end

  test "should create storage_device" do
    assert_difference('StorageDevice.count') do
      post storage_devices_url, params: { storage_device: { description: @storage_device.description, format: @storage_device.format, name: @storage_device.name, size: @storage_device.size } }, as: :json
    end

    assert_response 201
  end

  test "should show storage_device" do
    get storage_device_url(@storage_device), as: :json
    assert_response :success
  end

  test "should update storage_device" do
    patch storage_device_url(@storage_device), params: { storage_device: { description: @storage_device.description, format: @storage_device.format, name: @storage_device.name, size: @storage_device.size } }, as: :json
    assert_response 200
  end

  test "should destroy storage_device" do
    assert_difference('StorageDevice.count', -1) do
      delete storage_device_url(@storage_device), as: :json
    end

    assert_response 204
  end
end
