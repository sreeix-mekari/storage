Rails.application.routes.draw do
  resources :storage_devices
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  get 'ping', to: 'status#ping'
  get 'status', to: 'status#status'

end
